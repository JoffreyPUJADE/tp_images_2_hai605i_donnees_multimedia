#ifndef TRANSFORMATIONS_HPP
#define TRANSFORMATIONS_HPP

#include "image_ppm.h"
#include <algorithm>
#include <vector>

/*!
 * \brief Seuille une image en niveau de gris.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à seuiller.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image seuillée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \param [in] S (int) : Seuil de l'image.
 * \details Cette fonction permet de seuiller une image en niveau de gris.
*/
static inline void seuillageGris(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int S)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			if(ImgIn[i*nW+j] < S)
			{
				ImgOut[i*nW+j] = 0;
				
				if(ImgIn[i*nW+j - 1] < S)
					ImgOut[i*nW+j - 1] = 0;
				if(ImgIn[i*nW+j + 1] < S)
					ImgOut[i*nW+j + 1] = 0;
			}
			else
			{
				ImgOut[i*nW+j] = 255;
			}
		}
	}
}

/*!
 * \brief Seuille une image en niveau de gris.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à seuiller.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image seuillée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \param [in] S1 (int) : Premier seuil de l'image.
 * \param [in] S2 (int) : Second seuil de l'image.
 * \details Cette fonction permet de seuiller une image en niveau de gris avec deux seuils.
*/
static inline void seuillageGris2Seuils(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int S1, int S2)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			if(ImgIn[i*nW+j] < S1)
				ImgOut[i*nW+j] = 0;
			else if(ImgIn[i*nW+j] < S2)
				ImgOut[i*nW+j]=128;
			else
				ImgOut[i*nW+j] = 255;
		}
	}
}

/*!
 * \brief Seuille une image en niveau de gris.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à seuiller.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image seuillée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \param [in] S1 (int) : Premier seuil de l'image.
 * \param [in] S2 (int) : Second seuil de l'image.
 * \param [in] S3 (int) : Troisième seuil de l'image.
 * \details Cette fonction permet de seuiller une image en niveau de gris avec trois seuils.
*/
static inline void seuillageGris3Seuils(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int S1, int S2, int S3)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			if(ImgIn[i*nW+j] < S1)
				ImgOut[i*nW+j]=0;
			else if(ImgIn[i*nW+j] < S2)
				ImgOut[i*nW+j]=85;
			else if(ImgIn[i*nW+j] < S3)
				ImgOut[i*nW+j]=170;
			else
				ImgOut[i*nW+j]=255;

		}
	}
}

/*!
 * \brief Crée un histogramme d'une image en niveau de gris.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, dont on doit obtenir l'histogramme.
 * \param [in] nTaille (int) : Taille de l'image.
 * \details Cette fonction permet de créer les données d'un histogramme d'une image en niveau de gris.
 * \return Données de l'histogramme (std::vector<int>).
 */
static inline std::vector<int> histogrammeImage(OCTET* ImgIn, int nTaille)
{
	std::vector<int> nbOccurences;
	
	nbOccurences.resize(256);
	
	std::fill(nbOccurences.begin(), nbOccurences.end(), 0);
	
	for(int i=0;i<nTaille;++i)
	{
		++nbOccurences[((int)ImgIn[i])];
	}
	
	return nbOccurences;
}

/*!
 * \brief Crée un profil pour une image en niveau de gris.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à dont on doit créer un profil.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de créer le profil d'une ligne ou d'une colonne d'une image en niveau de gris.
 * \return Données du profil (std::vector<unsigned int>).
 */
static inline std::vector<unsigned int> profilImage(OCTET* ImgIn, int nH, int nW, char typeChoix, int indice)
{
	std::vector<unsigned int> tabProfil;
	
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			if(typeChoix == 'c')
			{
				if(j == indice)
					tabProfil.push_back(static_cast<unsigned int>(ImgIn[i]));
			}
			else if(typeChoix == 'l')
			{
				if(i == indice)
					tabProfil.push_back(static_cast<unsigned int>(ImgIn[j]));
			}
		}
	}
	
	return tabProfil;
}

/*!
 * \brief Seuille une image en couleur.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à seuiller.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image seuillée.
 * \param [in] nTaille3 (int) : Taille de l'image en couleur.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \param [in] S_R (int) : Seuil pour la couleur rouge de l'image.
 * \param [in] S_G (int) : Seuil pour la couleur verte de l'image.
 * \param [in] S_B (int) : Seuil pour la couleur bleue de l'image.
 * \details Cette fonction permet de seuiller une image en niveau de gris.
*/
static inline void seuillageCouleur(OCTET* ImgIn, OCTET* ImgOut, int nTaille3, int S_R, int S_G, int S_B)
{
	for (int i=0;i<nTaille3;i+=3)
	{
		int nR = ImgIn[i];
		int nG = ImgIn[i+1];
		int nB = ImgIn[i+2];
		
		if(nR < S_R)
			ImgOut[i]=0;
		else
			ImgOut[i]=255;
		
		if(nG < S_G)
			ImgOut[i+1]=0;
		else
			ImgOut[i+1]=255;
		
		if(nB < S_B)
			ImgOut[i+2]=0;
		else
			ImgOut[i+2]=255;
	}
}

/*!
 * \brief Crée un histogramme d'une image en couleur.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, dont on doit obtenir l'histogramme.
 * \param [in] nTaille3 (int) : Taille de l'image en couleur.
 * \details Cette fonction permet de créer les données d'un histogramme d'une image en couleur.
 * \return Données de l'histogramme (std::vector<std::vector<int> >).
 */
static inline std::vector<std::vector<int> > histogrammeCouleurImage(OCTET* ImgIn, int nTaille3)
{
	std::vector<std::vector<int> > nbOccurences;
	
	nbOccurences.push_back(std::vector<int>());
	nbOccurences.push_back(std::vector<int>());
	nbOccurences.push_back(std::vector<int>());
	
	int nbOccurencesRouge[256] = { 0 };
	int nbOccurencesVert[256] = { 0 };
	int nbOccurencesBleu[256] = { 0 };
	
	for(int i=0;i<nTaille3;i+=3)
	{
		++nbOccurencesRouge[((int)ImgIn[i])];
		++nbOccurencesVert[((int)ImgIn[i+1])];
		++nbOccurencesBleu[((int)ImgIn[i+2])];
	}
	
	for(size_t i=0;i<256;++i)
	{
		nbOccurences[0].push_back(nbOccurencesRouge[i]);
		nbOccurences[1].push_back(nbOccurencesVert[i]);
		nbOccurences[2].push_back(nbOccurencesBleu[i]);
	}
	
	return nbOccurences;
}

/*!
 * \brief Érode une image en noir et blanc.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à éroder.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image érodée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de réaliser une érosion sur une image en noir et blanc.
*/
static inline void erosionImage(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW)
{
	for(int y=1;y<(nH-2);++y)
	{
		for(int x=1;x<(nW-2);++x)
		{
			bool resultat = ((ImgIn[(y-1)*nW+x]) && (ImgIn[y*nW+(x-1)]) && (ImgIn[y*nW+(x+1)]) && (ImgIn[(y+1)*nW+x]));
			
			ImgOut[y*nW+x] = ((resultat) ? 255 : 0);
		}
	}
}

/*!
 * \brief Dilate une image en noir et blanc.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à dilater.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image dilatée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de réaliser une dilatation sur une image en noir et blanc.
*/
static inline void dilatationImage(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW)
{
	for(int y=1;y<(nH-2);++y)
	{
		for(int x=1;x<(nW-2);++x)
		{
			bool resultat = ((!ImgIn[(y-1)*nW+x]) || (!ImgIn[y*nW+(x-1)]) || (!ImgIn[y*nW+(x+1)]) || (!ImgIn[(y+1)*nW+x]));
			
			ImgOut[y*nW+x] = ((resultat) ? 0 : 255);
		}
	}
}

/*!
 * \brief Ferme une image en noir et blanc.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à fermer.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image fermée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de réaliser une fermeture sur une image en noir et blanc.
*/
static inline void fermetureImage(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int nTaille)
{
	dilatationImage(ImgIn, ImgOut, nH, nW);
	
	for(int i=0;i<nTaille;++i)
		ImgIn[i] = ImgOut[i];
	
	erosionImage(ImgIn, ImgOut, nH, nW);
}

/*!
 * \brief Ouvre une image en noir et blanc.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à ouvrir.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image ouverte.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de réaliser une ouverture sur une image en noir et blanc.
*/
static inline void ouvertureImage(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int nTaille)
{
	erosionImage(ImgIn, ImgOut, nH, nW);
	
	for(int i=0;i<nTaille;++i)
		ImgIn[i] = ImgOut[i];
	
	dilatationImage(ImgIn, ImgOut, nH, nW);
}

/*!
 * \brief Réalise le contour d'une image en noir et blanc.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à détourer.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image détourée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de réaliser une différrence sur une image en noir et blanc.
*/
static inline void differenceImage(OCTET* ImgInSeuillee, OCTET* ImgInDilatee, OCTET* ImgOut, int nH, int nW)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			bool test = (((ImgInSeuillee[i*nW+j] == 255) && (ImgInDilatee[i*nW+j] == 255)) || ((ImgInSeuillee[i*nW+j] == 0) && (ImgInDilatee[i*nW+j] == 0)));
			
			ImgOut[i*nW+j] = ((test) ? 255 : 0);
		}
	}
}

/*!
 * \brief Érode une couleur d'une image.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à éroder.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image érodée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de réaliser une érosion sur une couleur d'une image.
*/
static inline void erosionUneCouleurImage(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW)
{
	for(int y=1;y<(nH-2);++y)
	{
		for(int x=1;x<(nW-2);++x)
		{
			int resultat = ((ImgIn[(y-1)*nW+x]) & (ImgIn[y*nW+(x-1)]) & (ImgIn[y*nW+(x+1)]) & (ImgIn[(y+1)*nW+x]));
			
			ImgOut[y*nW+x] = resultat;
		}
	}
}

/*!
 * \brief Dilate une couleur d'une image.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à dilater.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image dilatée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de réaliser une dilatation sur une couleur d'une image.
*/
static inline void dilatationUneCouleurImage(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW)
{
	for(int y=1;y<(nH-2);++y)
	{
		for(int x=1;x<(nW-2);++x)
		{
			int resultat = ((ImgIn[(y-1)*nW+x]) | (ImgIn[y*nW+(x-1)]) | (ImgIn[y*nW+(x+1)]) | (ImgIn[(y+1)*nW+x]));
			
			ImgOut[y*nW+x] = resultat;
		}
	}
}

/*!
 * \brief Ferme une couleur d'une image.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à fermer.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image fermée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de réaliser une fermeture sur une image en niveaux de gris.
*/
static inline void fermetureGrisImage(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int nTaille)
{
	dilatationUneCouleurImage(ImgIn, ImgOut, nH, nW);
	
	for(int i=0;i<nTaille;++i)
		ImgIn[i] = ImgOut[i];
	
	erosionUneCouleurImage(ImgIn, ImgOut, nH, nW);
}

/*!
 * \brief Ouvre une couleur d'une image.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à ouvrir.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image ouverte.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de réaliser une ouverture sur une image en niveaux de gris.
*/
static inline void ouvertureGrisImage(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int nTaille)
{
	erosionUneCouleurImage(ImgIn, ImgOut, nH, nW);
	
	for(int i=0;i<nTaille;++i)
		ImgIn[i] = ImgOut[i];
	
	dilatationUneCouleurImage(ImgIn, ImgOut, nH, nW);
}

/*!
 * \brief Érode une image en couleur.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à éroder.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image érodée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de réaliser une érosion sur une image en couleur.
*/
static inline void erosionCouleurImage(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int nTaille)
{
	OCTET *tabRouge, *tabVert, *tabBleu;
	
	allocation_tableau(tabRouge, OCTET, nTaille);
	allocation_tableau(tabVert, OCTET, nTaille);
	allocation_tableau(tabBleu, OCTET, nTaille);
	
	planR(tabRouge, ImgIn, nTaille);
	planV(tabVert, ImgIn, nTaille);
	planB(tabBleu, ImgIn, nTaille);
	
	OCTET *tabFlouRouge, *tabFlouVert, *tabFlouBleu;
	
	allocation_tableau(tabFlouRouge, OCTET, nTaille);
	allocation_tableau(tabFlouVert, OCTET, nTaille);
	allocation_tableau(tabFlouBleu, OCTET, nTaille);
	
	erosionUneCouleurImage(tabRouge, tabFlouRouge, nH, nW);
	erosionUneCouleurImage(tabVert, tabFlouVert, nH, nW);
	erosionUneCouleurImage(tabBleu, tabFlouBleu, nH, nW);
	
	for(int i=0;i<nTaille;++i)
	{
		ImgOut[3*i+0] = tabFlouRouge[i];
		ImgOut[3*i+1] = tabFlouVert[i];
		ImgOut[3*i+2] = tabFlouBleu[i];
	}
}

/*!
 * \brief Dilate une image en couleur.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à dilater.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image dilatée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de réaliser une dilatation sur une image en couleur.
*/
static inline void dilatationCouleurImage(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int nTaille)
{
	OCTET *tabRouge, *tabVert, *tabBleu;
	
	allocation_tableau(tabRouge, OCTET, nTaille);
	allocation_tableau(tabVert, OCTET, nTaille);
	allocation_tableau(tabBleu, OCTET, nTaille);
	
	planR(tabRouge, ImgIn, nTaille);
	planV(tabVert, ImgIn, nTaille);
	planB(tabBleu, ImgIn, nTaille);
	
	OCTET *tabFlouRouge, *tabFlouVert, *tabFlouBleu;
	
	allocation_tableau(tabFlouRouge, OCTET, nTaille);
	allocation_tableau(tabFlouVert, OCTET, nTaille);
	allocation_tableau(tabFlouBleu, OCTET, nTaille);
	
	dilatationUneCouleurImage(tabRouge, tabFlouRouge, nH, nW);
	dilatationUneCouleurImage(tabVert, tabFlouVert, nH, nW);
	dilatationUneCouleurImage(tabBleu, tabFlouBleu, nH, nW);
	
	for(int i=0;i<nTaille;++i)
	{
		ImgOut[3*i+0] = tabFlouRouge[i];
		ImgOut[3*i+1] = tabFlouVert[i];
		ImgOut[3*i+2] = tabFlouBleu[i];
	}
}

/*!
 * \brief Ferme une image en couleur.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à fermer.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image fermée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de réaliser une fermeture sur une image en couleur.
*/
static inline void fermetureCouleurImage(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int nTaille)
{
	dilatationCouleurImage(ImgIn, ImgOut, nH, nW, nTaille);
	
	for(int i=0;i<(nTaille*3);++i)
		ImgIn[i] = ImgOut[i];
	
	erosionCouleurImage(ImgIn, ImgOut, nH, nW, nTaille);
}

/*!
 * \brief Ouvre une image en couleur.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à ouvrir.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image ouverte.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de réaliser une ouverture sur une image en couleur.
*/
static inline void ouvertureCouleurImage(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int nTaille)
{
	erosionCouleurImage(ImgIn, ImgOut, nH, nW, nTaille);
	
	for(int i=0;i<(nTaille*3);++i)
		ImgIn[i] = ImgOut[i];
	
	dilatationCouleurImage(ImgIn, ImgOut, nH, nW, nTaille);
}

#endif // TRANSFORMATIONS_HPP