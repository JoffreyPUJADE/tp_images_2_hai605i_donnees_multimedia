#include <iostream>
#include <stdio.h>
#include <sstream>
#include <string>
#include <stdexcept>
#include "Transformations.hpp"

template<typename T, typename S>
S type2Other(T valeur)
{
	std::ostringstream oss;
	
	oss << valeur;
	
	std::istringstream iss(oss.str());
	
	S resultat;
	
	iss >> resultat;
	
	return resultat;
}

int main(int argc, char* argv[])
{
	int nH, nW, nTaille;

	if(argc != 3)
		throw std::runtime_error("Usage: ImageIn.pgm ImageOut.pgm");
	
	std::string nomImageLue = argv[1];
	std::string nomImageEcrite = argv[2];
	
	OCTET *ImgIn, *ImgOut;
	
	lire_nb_lignes_colonnes_image_pgm(nomImageLue.c_str(), &nH, &nW);
	
	nTaille = nH * nW;
	
	allocation_tableau(ImgIn, OCTET, nTaille);
	lire_image_pgm(nomImageLue.c_str(), ImgIn, nH * nW);
	allocation_tableau(ImgOut, OCTET, nTaille);
	
	fermetureImage(ImgIn, ImgOut, nH, nW, nTaille);
	
	ecrire_image_pgm(nomImageEcrite.c_str(), ImgOut, nH, nW);
	
	free(ImgIn);
	free(ImgOut);
	
	return 0;
}
