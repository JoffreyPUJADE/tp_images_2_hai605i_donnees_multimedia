#include <iostream>
#include <stdio.h>
#include <sstream>
#include <string>
#include <stdexcept>
#include "Transformations.hpp"

template<typename T, typename S>
S type2Other(T valeur)
{
	std::ostringstream oss;
	
	oss << valeur;
	
	std::istringstream iss(oss.str());
	
	S resultat;
	
	iss >> resultat;
	
	return resultat;
}

int main(int argc, char* argv[])
{
	int nH, nW, nTaille;

	if(argc != 4)
		throw std::runtime_error("Usage: ImageInSeuillee.pgm ImgInDilatee.pgm ImageOut.pgm");
	
	std::string nomImageLue1 = argv[1];
	std::string nomImageLue2 = argv[2];
	std::string nomImageEcrite = argv[3];
	
	OCTET *ImgInSeuillee, *ImgInDilatee, *ImgOut;
	
	lire_nb_lignes_colonnes_image_pgm(nomImageLue1.c_str(), &nH, &nW);
	
	nTaille = nH * nW;
	
	allocation_tableau(ImgInSeuillee, OCTET, nTaille);
	lire_image_pgm(nomImageLue1.c_str(), ImgInSeuillee, nH * nW);
	allocation_tableau(ImgInDilatee, OCTET, nTaille);
	lire_image_pgm(nomImageLue2.c_str(), ImgInDilatee, nH * nW);
	allocation_tableau(ImgOut, OCTET, nTaille);
	
	differenceImage(ImgInSeuillee, ImgInDilatee, ImgOut, nH, nW);
	
	ecrire_image_pgm(nomImageEcrite.c_str(), ImgOut, nH, nW);
	
	free(ImgInSeuillee);
	free(ImgInDilatee);
	free(ImgOut);
	
	return 0;
}
